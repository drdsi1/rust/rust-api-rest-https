#!/bin/bash

if ! [ -d /opt/keys ]; then
    sudo mkdir /opt/keys
    sudo chown $USER /opt/keys
else
    rm -rf /opt/keys/*
fi

openssl req -x509 -newkey rsa:4096 -nodes -keyout /opt/keys/key.pem -out /opt/keys/cert.pem -days 365 -subj '/CN=192.168.36.145'

openssl req  -nodes -new -x509 -keyout /opt/keys/server.key -out /opt/keys/server.cert -days 365 -sha256 -subj "/C=BR/ST=MG/L=Betim/O=DRDSI/OU=Labs/CN=labs.lxd"