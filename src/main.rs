use actix_web::{get, App, HttpRequest, HttpServer, Responder};
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

use std::process::Command;

#[get("/")]
async fn index(_req: HttpRequest) -> impl Responder {
    "Welcome!"
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let mut command = Command::new("bash");
    command
        .arg("sudo chmod +x generate-keys.sh")
        .arg("./generate-keys.sh");

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    
    builder
        .set_private_key_file("/opt/keys/key.pem", SslFiletype::PEM)
        .unwrap();

    builder.set_certificate_chain_file("/opt/keys/cert.pem").unwrap();

    HttpServer::new(|| App::new().service(index))
        .bind_openssl("0.0.0.0:443", builder)?
        .run()
        .await
}